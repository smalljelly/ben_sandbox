// (c) Copyright HutongGames, LLC 2010-2013. All rights reserved.

using UnityEngine;

namespace HutongGames.PlayMaker.Actions
{
	[ActionCategory(ActionCategory.GameObject)]
	[Tooltip("Finds the Parent of a GameObject by Name. Use this to find attach points etc. NOTE: This action will search recursively through all parents and return the first match; To find a specific parent use Find Parent.")]
	public class GetParentByName : FsmStateAction
	{
		[RequiredField]
        [Tooltip("The GameObject to search.")]
		public FsmOwnerDefault owner;

        [Tooltip("The name of the parent to search for.")]
		public FsmString parentName;

		[RequiredField]
		[UIHint(UIHint.Variable)]
        [Tooltip("Store the result in a GameObject variable.")]
		public FsmGameObject storeResult;



		public override void OnEnter()
		{
			CheckParent( Fsm.GetOwnerDefaultTarget(owner).transform );
		}

		private void CheckParent( Transform currentObject )
		{
			if( currentObject.parent == null )
			{
				Finish();
				return;
			}

			if( currentObject.parent.name == parentName.Value )
			{
				storeResult.Value = currentObject.parent.gameObject;
				Finish();
				return;
			}

			CheckParent( currentObject.parent );
		}
	}
}